package dto

type NewCustomerRequest struct {
	Company       string `json:"company"`
	LastName      string `json:"lastName"`
	FirstName     string `json:"firstName"`
	Emailaddress  string `json:"emailAddress"`
	JobTitle      string `json:"jobTitle"`
	BusinessPhone string `json:"businessPhone"`
	HomePhone     string `json:"homePhone"`
	MobilePhone   string `json:"mobilePhone"`
	FaxNumber     string `json:"faxNumber"`
	Address       string `json:"address"`
	City          string `json:"city"`
	StateProvince string `json:"stateProvince"`
	ZipPostalCode string `json:"zipPostalCode"`
	CountryRegion string `json:"countryRegion"`
	WebPage       string `json:"webPage"`
	Notes         string `json:"notes"`
}