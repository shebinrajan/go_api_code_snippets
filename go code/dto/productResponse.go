package dto

type ProductResponse struct {
	SupplierId             string  `json:"supplierIds"`
	Id                     int     `json:"id"`
	ProductCode            string  `json:"productCode"`
	ProductName            string  `json:"productName"`
	Description            string  `json:"description"`
	StandardCost           float32 `json:"standardCost"`
	ListPrice              float32 `json:"listPrice"`
	ReorderLevel           int     `json:"reorderLevel"`
	TargetLevel            int     `json:"targetLevel"`
	QuantityPerUnit        string  `json:"quantityPerUnit"`
	Discontinued           string  `json:"discontinued"`
	MinimumReorderQuantity int     `json:"minimumReorderQuantity"`
	Category               string  `json:"category"`
}
