package domain

import (
	"database/sql"

	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking-lib/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type ProductRepositoryDb struct {
	client *sqlx.DB
}

func (d ProductRepositoryDb) FindAll() ([]Product, *errs.AppError) {
	findAllSql := "select id, list_price, discontinued from products"

	rows, err := d.client.Query(findAllSql)

	if err != nil {
		logger.Error("Error while querying products table " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected Database error")
	}
	products := make([]Product, 0)
	err = sqlx.StructScan(rows, &products)
	if err != nil {
		logger.Error("Error while scanning  products" + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected Database error")
	}
	return products, nil
}

func (d ProductRepositoryDb) ById(id string) (*Product, *errs.AppError) {
	productSql := " select id, list_price, discontinued from products where id = ? "

	var p Product
	err := d.client.Get(&p, productSql, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("Product not found")
		} else {
			logger.Error("Error while scanning product " + err.Error())
			return nil, errs.NewUnexpectedError("Unexpected database error")
		}
	}
	return &p, nil
}

func NewProductRepositoryDb(dbClient *sqlx.DB) ProductRepositoryDb {
	return ProductRepositoryDb{dbClient}
}
