package domain

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/dto"
)

type PremiumCustomer struct {
	Id            int    `db:"id"`
	Company       string `db:"company"`
	LastName      string `db:"last_name"`
	FirstName     string `db:"first_name"`
	Emailaddress  string `db:"email_address"`
	JobTitle      string `db:"job_title"`
	BusinessPhone string `db:"business_phone"`
	HomePhone     string `db:"home_phone"`
	MobilePhone   string `db:"mobile_phone"`
	FaxNumber     string `db:"fax_number"`
	Address       string `db:"address"`
	City          string `db:"city"`
	StateProvince string `db:"state_province"`
	ZipPostalCode string `db:"zip_postal_code"`
	CountryRegion string `db:"country_region"`
	WebPage       string `db:"web_page"`
	Notes         string `db:"notes"`
	//Attachments		string	`db:"attachments"`
}

/*
func (c premiumCustomer) statusAsText() string {
	statusAsText := "active"
	if c.Status == "0" {
		statusAsText = "inactive"
	}
	return statusAsText
}
*/
func (c PremiumCustomer) ToDto() dto.PremiumCustomerResponse {
	return dto.PremiumCustomerResponse{
		Id:            c.Id,
		Company:       c.Company,
		LastName:      c.LastName,
		FirstName:     c.FirstName,
		Emailaddress:  c.Emailaddress,
		JobTitle:      c.JobTitle,
		BusinessPhone: c.BusinessPhone,
		HomePhone:     c.HomePhone,
		MobilePhone:   c.MobilePhone,
		FaxNumber:     c.FaxNumber,
		Address:       c.Address,
		City:          c.City,
		StateProvince: c.StateProvince,
		ZipPostalCode: c.ZipPostalCode,
		CountryRegion: c.CountryRegion,
		WebPage:       c.WebPage,
		Notes:         c.Notes,
	}
}

type PremiumCustomerRepository interface {
	FindAll(id string) ([]PremiumCustomer, *errs.AppError)
	ById(string) (*PremiumCustomer, *errs.AppError)
}
