package domain

import (
	"database/sql"

	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking-lib/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type PremiumCustomerRepositoryDb struct {
	client *sqlx.DB
}

func (d PremiumCustomerRepositoryDb) FindAll(status string) ([]PremiumCustomer, *errs.AppError) {
	var err error
	premiumCustomers := make([]PremiumCustomer, 0)

		findAllSql := `select 
						id
						,COALESCE(company, '') as company
						,COALESCE(last_name, '') as last_name
						,COALESCE(first_name, 0) as first_name
						,COALESCE(email_address, 0) as email_address
						,COALESCE(job_title, 0) as job_title
						,COALESCE(business_phone, '') as business_phone
						,COALESCE(home_phone, '') as home_phone
						,COALESCE(mobile_phone, 0) as mobile_phone
						,COALESCE(fax_number, 0) as fax_number
						,COALESCE(city, '') as city
						,COALESCE(state_province, '') as state_province
						,COALESCE(zip_postal_code, '') as zip_postal_code
						,COALESCE(country_region, '') as country_region
						,COALESCE(web_page, '') as web_page
						,COALESCE(notes, '') as notes
						from customers`
		err = d.client.Select(&premiumCustomers, findAllSql)

	if err != nil {
		logger.Error("Error while querying customers table " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}

	return premiumCustomers, nil
}

func (d PremiumCustomerRepositoryDb) ById(id string) (*PremiumCustomer, *errs.AppError) {
	premiumCustomerSql := `select 
							id
							,COALESCE(company, '') as company
							,COALESCE(last_name, '') as last_name
							,COALESCE(first_name, 0) as first_name
							,COALESCE(email_address, 0) as email_address
							,COALESCE(job_title, 0) as job_title
							,COALESCE(business_phone, '') as business_phone
							,COALESCE(home_phone, '') as home_phone
							,COALESCE(mobile_phone, 0) as mobile_phone
							,COALESCE(fax_number, 0) as fax_number
							,COALESCE(city, '') as city
							,COALESCE(state_province, '') as state_province
							,COALESCE(zip_postal_code, '') as zip_postal_code
							,COALESCE(country_region, '') as country_region
							,COALESCE(web_page, '') as web_page
							,COALESCE(notes, '') as notes
							from customers 
							where id = ?`

	var c PremiumCustomer
	err := d.client.Get(&c, premiumCustomerSql, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("Customer not found")
		} else {
			logger.Error("Error while scanning customer " + err.Error())
			return nil, errs.NewUnexpectedError("Unexpected database error")
		}
	}
	return &c, nil
}

func NewPremiumCustomerRepositoryDb(dbClient *sqlx.DB) PremiumCustomerRepositoryDb {
	return PremiumCustomerRepositoryDb{dbClient}
}
