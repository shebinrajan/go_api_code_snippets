package domain

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking-lib/logger"
	"github.com/jmoiron/sqlx"
)

type NewCustomerEntryRepositoryDb struct {
	client *sqlx.DB
}

func (d NewCustomerEntryRepositoryDb) Save(a NewCustomer) (*NewCustomer, *errs.AppError) {
	sqlInsert := "INSERT INTO customers (company, last_name, first_name, email_address, job_title, business_phone, home_phone, mobile_phone, fax_number, address, city, state_province, zip_postal_code, country_region, web_page, notes) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	result, err := d.client.Exec(sqlInsert, a.Company, a.LastName, a.FirstName, a.Emailaddress, a.JobTitle, a.BusinessPhone, a.HomePhone, a.MobilePhone, a.FaxNumber, a.Address, a.City, a.StateProvince, a.ZipPostalCode, a.CountryRegion, a.WebPage, a.Notes,)
	if err != nil {
		logger.Error("Error while creating new customer: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}

	id, err := result.LastInsertId()
	if err != nil {
		logger.Error("Error while getting last insert id for new customer: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}
	a.Id = int(id)
	return &a, nil
}


func NewCustomerentryRepositoryDb(dbClient *sqlx.DB) NewCustomerEntryRepositoryDb {
	return NewCustomerEntryRepositoryDb{dbClient}
}
