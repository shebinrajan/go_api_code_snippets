package domain

import (
	"github.com/ashishjuyal/banking/dto"
	"github.com/ashishjuyal/banking-lib/errs"
)


type NewCustomer struct {
	Id            int    `db:"id"`
	Company       string `db:"company"`
	LastName      string `db:"last_name"`
	FirstName     string `db:"first_name"`
	Emailaddress  string `db:"email_address"`
	JobTitle      string `db:"job_title"`
	BusinessPhone string `db:"business_phone"`
	HomePhone     string `db:"home_phone"`
	MobilePhone   string `db:"mobile_phone"`
	FaxNumber     string `db:"fax_number"`
	Address       string `db:"address"`
	City          string `db:"city"`
	StateProvince string `db:"state_province"`
	ZipPostalCode string `db:"zip_postal_code"`
	CountryRegion string `db:"country_region"`
	WebPage       string `db:"web_page"`
	Notes         string `db:"notes"`
}

func (c NewCustomer) TonewCustomerResponseDto() *dto.NewCustomerResponse {
	return &dto.NewCustomerResponse{c.Id}
}

//go:generate mockgen -destination=../mocks/domain/mocknewCustomerRepository.go -package=domain github.com/ashishjuyal/banking/domain newCustomerRepository
type NewCustomerRepository interface {
	Save(NewCustomer NewCustomer) (*NewCustomer, *errs.AppError)
	FindBy(Id string) (*NewCustomer, *errs.AppError)
}


func NewentryCustomer(Company,LastName,FirstName,Emailaddress,JobTitle,BusinessPhone,HomePhone,MobilePhone,FaxNumber,Address, City,StateProvince,ZipPostalCode,CountryRegion,WebPage, Notes string,) NewCustomer {
	return NewCustomer{
		Company:       Company,
		LastName:      LastName,
		FirstName:     FirstName,
		Emailaddress:  Emailaddress,
		JobTitle:      JobTitle,
		BusinessPhone: BusinessPhone,
		HomePhone:     HomePhone,
		MobilePhone:   MobilePhone,
		FaxNumber:     FaxNumber,
		Address:       Address,
		City:          City,
		StateProvince: StateProvince,
		ZipPostalCode: ZipPostalCode,
		CountryRegion: CountryRegion,
		WebPage:       WebPage,
		Notes:         Notes,
	}
}
