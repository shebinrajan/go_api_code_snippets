package service

import (
	"github.com/ashishjuyal/banking/domain"
	"github.com/ashishjuyal/banking/dto"
	"github.com/ashishjuyal/banking-lib/errs"
)

//go:generate mockgen -destination=../mocks/service/mockCustomerService.go -package=service github.com/ashishjuyal/banking/service CustomerService
type PremiumCustomerService interface {
	GetAllpremiumCustomer(string) ([]dto.PremiumCustomerResponse, *errs.AppError)
	GetpremiumCustomer(string) (*dto.PremiumCustomerResponse, *errs.AppError)
}

type DefaultpremiumCustomerService struct {
	repo domain.PremiumCustomerRepository
}

func (s DefaultpremiumCustomerService) GetAllpremiumCustomer(status string) ([]dto.PremiumCustomerResponse, *errs.AppError) {

	premiumCustomers, err := s.repo.FindAll(status)
	if err != nil {
		return nil, err
	}
	response := make([]dto.PremiumCustomerResponse, 0)
	for _, c := range premiumCustomers {
		response = append(response, c.ToDto())
	}
	return response, err
}

func (s DefaultpremiumCustomerService) GetpremiumCustomer(id string) (*dto.PremiumCustomerResponse, *errs.AppError) {
	c, err := s.repo.ById(id)
	if err != nil {
		return nil, err
	}
	response := c.ToDto()
	return &response, nil
}

func NewPreminumCustomerService(repository domain.PremiumCustomerRepository) DefaultpremiumCustomerService {
	return DefaultpremiumCustomerService{repository}
}