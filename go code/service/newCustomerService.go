package service

import (
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/domain"
	"github.com/ashishjuyal/banking/dto"
)


type NewCustomerEntryService interface {
	NewCustomer(request dto.NewCustomerRequest) (*dto.NewCustomerResponse, *errs.AppError)
}

type DefaultnewCustomerService struct {
	repo domain.NewCustomerEntryRepositoryDb
}

func (s DefaultnewCustomerService) NewCustomer(req dto.NewCustomerRequest) (*dto.NewCustomerResponse, *errs.AppError) {

	NewCustomer := domain.NewentryCustomer(req.Company, 
		req.LastName, req.FirstName, req.Emailaddress, 
		req.JobTitle, req.BusinessPhone, req.HomePhone, 
		req.MobilePhone, req.FaxNumber, req.Address, 
		req.City, req.StateProvince, req.ZipPostalCode, 
		req.CountryRegion, req.WebPage, req.Notes)
		
	if NewCustomer, err := s.repo.Save(NewCustomer); err != nil {
		return nil, err
	} else {
		return NewCustomer.TonewCustomerResponseDto(), nil
	}
}


func NewCustomerentryService(repo domain.NewCustomerEntryRepositoryDb) DefaultnewCustomerService {
	return DefaultnewCustomerService{repo}
}
