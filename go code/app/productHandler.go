package app

import (
	_"encoding/json"
	_"github.com/ashishjuyal/banking/dto"
	"github.com/ashishjuyal/banking/service"
	"github.com/gorilla/mux"
	"net/http"
)

type ProductHandlers struct {
	service service.ProductService
}

func (ph *ProductHandlers) getAllProducts(w http.ResponseWriter, r *http.Request) {

	id := r.URL.Query().Get("id")

	products, err := ph.service.GetAllProduct(id)

	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, products)
	}
}

func (ph *ProductHandlers) getProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	product, err := ph.service.GetProduct(id)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, product)
	}
}
/*
func writeResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(err)
	}
}
*/