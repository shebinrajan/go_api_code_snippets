package app

import (
	"net/http"

	"github.com/ashishjuyal/banking/service"
	"github.com/gorilla/mux"
)

type ProductHandlers struct {
	service service.ProductService
}

func (ch *ProductHandlers) getAllProducts(w http.ResponseWriter, r *http.Request) {

	discontinued := r.URL.Query().Get("discontinued")

	products, err := ch.service.GetAllProduct(discontinued)

	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, products)
	}
}


func (ch *ProductHandlers) getProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	customer, err := ch.service.GetProduct(id)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, customer)
	}
}

/*
func writeProductResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(err)
	}
}
*/
