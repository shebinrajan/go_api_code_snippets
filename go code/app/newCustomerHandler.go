package app

import (
	"encoding/json"
	"github.com/ashishjuyal/banking/dto"
	"github.com/ashishjuyal/banking/service"
	"github.com/gorilla/mux"
	"net/http"
)

type NewCustomerHandler struct {
	service service.NewCustomerEntryService
}

func (h NewCustomerHandler) NewCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	Company := vars["company"]
	var request dto.NewCustomerRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		request.Company = Company
		NewCustomer, appError := h.service.NewCustomer(request)
		if appError != nil {
			writeResponse(w, appError.Code, appError.AsMessage())
		} else {
			writeResponse(w, http.StatusCreated, NewCustomer)
		}
	}
}


