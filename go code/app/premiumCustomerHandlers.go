package app

import (
	_"encoding/json"
	"github.com/ashishjuyal/banking/service"
	"github.com/gorilla/mux"
	"net/http"
)

type PremiumCustomerHandlers struct {
	service service.PremiumCustomerService
}

func (pch *PremiumCustomerHandlers) getAllpremiumCustomers(w http.ResponseWriter, r *http.Request) {

	status := r.URL.Query().Get("id")

	premiumCustomers, err := pch.service.GetAllpremiumCustomer(status)

	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, premiumCustomers)
	}
}

func (pch *PremiumCustomerHandlers) getpremiumCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	premiumCustomer, err := pch.service.GetpremiumCustomer(id)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, premiumCustomer)
	}
}
/*
func writeResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(err)
	}
}
*/	