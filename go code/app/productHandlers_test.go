package app

import (
	"github.com/ashishjuyal/banking/dto"
	"github.com/ashishjuyal/banking-lib/errs"
	"github.com/ashishjuyal/banking/mocks/service"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"testing"
)

var router *mux.Router
var ph ProductHandlers
var mockService *service.MockProductService

func setup(t *testing.T) func() {
	ctrl := gomock.NewController(t)
	mockService = service.NewMockCProductService(ctrl)
	ph = ProductHandlers{mockService}
	router = mux.NewRouter()
	router.HandleFunc("/products", ch.getAllProducts)
	return func() {
		router = nil
		defer ctrl.Finish()
	}
}

func Test_should_return_products_with_status_code_200(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	dummyProducts := []dto.ProductResponse{
		{"4", "1", "NWTB-1", "Northwind Traders Chai", "NULL", "13.5000", "18.0000", "10", "40", "10 boxes x 20 bags", "0", "10", "Beverages", ""},
	}
	mockService.EXPECT().GetAllProduct("").Return(dummyProducts, nil)
	request, _ := http.NewRequest(http.MethodGet, "/products", nil)

	// Act
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, request)

	// Assert
	if recorder.Code != http.StatusOK {
		t.Error("Failed while testing the status code")
	}
}

func Test_should_return_status_code_500_with_error_message(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()
	mockService.EXPECT().GetAllProduct("").Return(nil, errs.NewUnexpectedError("some database error"))
	request, _ := http.NewRequest(http.MethodGet, "/products", nil)

	// Act
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, request)

	// Assert
	if recorder.Code != http.StatusInternalServerError {
		t.Error("Failed while testing the status code")
	}
}
